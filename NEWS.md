# squirrelsnicolas 0.0.0.9000

- add function get_message_fur_color()
- add function check_primary_color_is_ok()

* Added a `NEWS.md` file to track changes to the package.
