---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# squirrelsnicolas

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsnicolas is to analyse data with squirrels. It's an amazing package.
You can test it.

## Installation

You can install the development version of squirrelsnicolas like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(squirrelsnicolas)
## basic example code

check_primary_color_is_ok(string = c("Black","Cinnamon"))
```

